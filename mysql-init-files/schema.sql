CREATE DATABASE IF NOT EXISTS paipaidb
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_unicode_ci;

USE paipaidb;

DROP TABLE IF EXISTS user_points;
DROP TABLE IF EXISTS games;

CREATE TABLE games (
  id                  INT NOT NULL AUTO_INCREMENT,
  game_name           VARCHAR (255) NOT NULL,
  in_progress         BOOL NOT NULL,
  waiting_for_players BOOL NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE user_points (
  id                  INT NOT NULL AUTO_INCREMENT,
  player_id           VARCHAR (255) NOT NULL,
  game_id             INT NOT NULL,
  points              INT,
  PRIMARY KEY (id),
  FOREIGN KEY (game_id) REFERENCES games(id)
);

INSERT INTO games(game_name, in_progress, waiting_for_players) VALUES ("Hangman", false, false);