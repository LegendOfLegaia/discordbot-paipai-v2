Versioning: z.y.x
z -> Major version update, requires downtime
y -> Requires database modifications and possible downtime
x -> New files, fixes, changes, etc. Nothing too noticeable

# v2.0.0
 * Converted PaiPai over to Python3 + MySQL

# v1.1.0
 * Migrated PaiPai to docker-compose for localized mongoDb

# v1.0.0
 * Initial creation of PaiPai with Hangman!