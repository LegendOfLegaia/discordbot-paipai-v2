from sqlalchemy import *
from sqlalchemy import ForeignKey
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

from src.models import game

Base = declarative_base()


class UserPoint(Base):
    """"""
    __tablename__ = "user_points"

    id = Column(Integer, primary_key=True)
    game_id = Column(Integer)
    player_id = Column(String)
    points = Column(Integer)

    def __init__(self, game_id, player_id, points):
        """"""
        self.game_id = game_id
        self.player_id = player_id
        self.points = points
