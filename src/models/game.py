from sqlalchemy import *
from sqlalchemy import ForeignKey
from sqlalchemy import Column, Date, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

Base = declarative_base()


class Game(Base):
    """"""
    __tablename__ = "games"

    id = Column(Integer, primary_key=True)
    game_name = Column(String)
    in_progress = Column(Boolean)
    waiting_for_players = Column(Boolean)

    def __init__(self, game_name, in_progress, waiting_for_players):
        """"""
        self.game_name = game_name
        self.in_progress = in_progress
        self.waiting_for_players = waiting_for_players
