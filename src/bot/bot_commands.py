import discord

from src.bot import player_points
from src.config import configuration
from src.helpers import string_helpers
from src.games import game_manager
from src.models import game, user_points

Client = None
Database = None


def get_game_name(command: str) -> str:
    """

    Splits the command in two by a dash, returning the 2nd word if it's exactly a split of 2

    Returns:
      None: if split is != 2
      Game Name: if split is == 2

    """
    game_name_split = command.split('-')
    if len(game_name_split) == 2:
        return game_name_split[1]
    return None


async def handle_message(message: object) -> None:
    """

    Parses the message and forwards it to the respective method, if applicable

    """

    if Client == None or Database == None:
        return

    message_content = message.content.split(' ')

    if len(message_content) > 0:
        game_command_ran = await run_game_command(message_content[0], message)
        if not game_command_ran:
            if message_content[0] == string_helpers.prepend_message_char('game'):
                await game_manager.handle_game_command(message, Database, Client)
            elif message_content[0] == string_helpers.prepend_message_char('help'):
                await show_help(message)
            elif message_content[0] == string_helpers.prepend_message_char('inprogress'):
                await show_in_progress(message)
            elif message_content[0] == string_helpers.prepend_message_char('marco'):
                await message.reply('polo')
            elif message_content[0] == string_helpers.prepend_message_char('mypoints'):
                await show_my_points(message)
            else:
                await message.reply("I don't know that command. Please type `"+string_helpers.prepend_message_char('help')+"` for options.")


async def run_game_command(command: str, message: object) -> bool:
    """

    Assuming the command is a game command, this will run the command

    Returns:
      True: A game command was found and was run
      False: No game command matches

    """
    game_name = get_game_name(command)

    if game_name:
        if command.startswith(string_helpers.prepend_message_char('top3-')):
            await player_points.get_top_three_for_game(message, game_name, Client, Database)
            return True
        elif command.startswith(string_helpers.prepend_message_char('join-')):
            await game_manager.join_game(message, Database, game_name)
            return True
        elif command.startswith(string_helpers.prepend_message_char('start-')):
            await game_manager.start_game(message, Database, Client, game_name)
            return True

    return False


async def show_help(message: object) -> None:
    """

    Prints out the help to the user

    """
    games_available_string = ''

    for game in game_manager.games_available:
        game_object = game_manager.games_available[game]
        games_available_string += f"<{game_object['emoji_id']}> {game_object['name']} - {game_object['description']}\n"

    embed = discord.Embed(
        color=3329330, title=f"<{configuration.EMOJIS['Gimard']}> PaiPai Rice Patty Menu <{configuration.EMOJIS['Vera']}>")
    embed.add_field(
        name="Current Games Available:",
        value=games_available_string,
        inline=False)
    embed.add_field(
        name="Do an action on the current game:",
        value="`{} [action]` => Do some kind of action for the game.".format(
            string_helpers.prepend_message_char('game')),
        inline=False)
    embed.add_field(
        name="Current Game:",
        value="`{}` => Display the status of the current games being played, if any.".format(
            string_helpers.prepend_message_char('inprogress')),
        inline=False)
    embed.add_field(
        name="Marco:",
        value="`{}` => Responds with polo.".format(
            string_helpers.prepend_message_char('marco')),
        inline=False)
    embed.add_field(
        name="Show My Points:",
        value="`{}` => Prints out how many points you've accumulated.".format(
            string_helpers.prepend_message_char('mypoints')),
        inline=False)
    embed.add_field(
        name="Join a Given Game:",
        value="`{0}-[game]` => Join a game that someone started. Example usage: `{0}-hangman`".format(
            string_helpers.prepend_message_char('join')),
        inline=False)
    embed.add_field(
        name="Start a Game:",
        value="`{0}-[game]` => Start a new game (if one isn't already taking place). Example usage: `{0}-hangman`".format(
            string_helpers.prepend_message_char('start')),
        inline=False)
    embed.add_field(
        name="Show Top Players:",
        value="`{0}-[game]` => Shows the top three high scores and the players for the respective game. Example usage: `{0}-hangman`".format(
            string_helpers.prepend_message_char('top3')),
        inline=False)

    await message.reply(embed=embed)


async def show_in_progress(message: object) -> None:
    """

    Shows the games that are in progress

    """
    in_progress_games_str = ""
    waiting_game_list_str = ""

    for in_progress_game in Database.query(game.Game).filter_by(in_progress=True):
        game_obj = game_manager.games_available[in_progress_game.game_name]
        in_progress_games_str += f"<{game_obj['emoji_id']}> {game_obj['name']}\n"

    for waiting_game in Database.query(game.Game).filter_by(waiting_for_players=True):
        game_obj = game_manager.games_available[waiting_game.game_name]
        waiting_game_list_str += f"<{game_obj['emoji_id']}> {game_obj['name']}\n"

    if in_progress_games_str == "":
        in_progress_games_str = "None!"
    if waiting_game_list_str == "":
        waiting_game_list_str = "None!"

    embed = discord.Embed(
        color=3329330,
        title='Games In Progress'
    )
    embed.set_author(name=Client.user.name, icon_url=Client.user.avatar_url)
    embed.add_field(name="These are the games in progress:",
                    value=in_progress_games_str, inline=False)
    embed.add_field(name="These are the games waiting for players to join:",
                    value=waiting_game_list_str, inline=False)

    await message.reply(embed=embed)


async def show_my_points(message: object) -> None:
    """

    Shows all the points for the user asking

    """
    all_games = Database.query(game.Game).order_by(game.Game.id)
    user_stats = Database.query(user_points.UserPoint).filter_by(
        player_id=str(message.author.id))

    embed = discord.Embed(
        color=3329330,
        title='Game Stats'
    )
    embed.set_author(name=Client.user.name, icon_url=Client.user.avatar_url)

    for found_game in all_games:
        respective_stat = user_stats.filter_by(game_id=found_game.id).first()
        if respective_stat == None:
            respective_stat = user_points.UserPoint(
                found_game.id, str(message.author.id), 0)
            Database.add(respective_stat)
            Database.commit()
        embed.add_field(name="{}:".format(found_game.game_name), value="You currently have {} points".format(
            respective_stat.points), inline=False)

    await message.reply(embed=embed)
