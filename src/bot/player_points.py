import discord

from src.models import game, user_points


async def get_top_three_for_game(message: object, game_name: str, client: object, database: object) -> None:
    """

    Responds to the user with the top 3 users for the respective game

    """
    respective_game = database.query(
        game.Game).filter_by(game_name=game_name).first()
    top_three = database.query(user_points.UserPoint).\
        filter_by(game_id=respective_game.id).\
        order_by(user_points.UserPoint.points.desc()).\
        limit(3).all()

    if len(top_three) == 0:
        await message.reply("Either there are no users playing that or the game doesn't exist!")
        return

    field_value = ""
    for top_player in top_three:
        user = await client.fetch_user(top_player.player_id)
        field_value += "{} -> {}\n".format(top_player.points,
                                           user.display_name)

    embed = discord.Embed(
        color=3329330,
        title='Top 3 Players for {}'.format(respective_game.game_name)
    )
    embed.set_author(name=client.user.name, icon_url=client.user.avatar_url)
    embed.add_field(name="Points -> Player Name",
                    value=field_value, inline=False)

    await message.reply(embed=embed)
