import asyncio

import discord
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.bot import bot_commands

from src.config import configuration, keys
from src.games import game_manager

BotLoop = None


class PaiPai(discord.Client):
    async def on_ready(self):
        global BotLoop
        BotLoop = asyncio.get_event_loop()
        bot_commands.Client = self
        print('Logged on as {0}!'.format(self.user))
        session = self.connect_to_database()
        await game_manager.restart_games(session)
        session.close()

    async def on_message(self, message):
        if len(message.content) == 0 or \
           message.content[0] != configuration.MESSAGE_CHARACTER or \
           message.author == self.user.id:
            return

        bot_commands.Database = self.connect_to_database()
        await bot_commands.handle_message(message)
        bot_commands.Database.close()

    def connect_to_database(self) -> object:
        engine = create_engine(
            'mysql+mysqlconnector://{}:{}@{}:3306/{}'.format(
                keys.DATABASE_USER, keys.DATABASE_PASSWORD, keys.DATABASE_URL, keys.DATABASE_NAME),
            echo=False  # True
        )

        Session = sessionmaker(bind=engine)
        return Session()
