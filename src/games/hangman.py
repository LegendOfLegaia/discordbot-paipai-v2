import asyncio
import discord
import json
from math import floor
import random
import re
import requests

from src.bot import bot
from src.config import configuration, keys
from src.helpers import string_helpers
from src.models import game, user_points


class Hangman:
    def clear_timeouts(self) -> None:
        if self.player_timeout != None:
            self.player_timeout.cancel()
            self.player_timeout = None
        if self.global_timeout != None:
            self.global_timeout.cancel()
            self.global_timeout = None

    async def game_over(self, channel: object, database: object) -> None:
        self.clear_timeouts()
        database.query(game.Game).filter_by(game_name="Hangman").update(
            values={game.Game.waiting_for_players: False, game.Game.in_progress: False})
        database.commit()
        await channel.send("Gameover!\n{}".format(self.show_hangman()))

    async def game_over_callback(self, channel: object, database: object) -> None:
        await asyncio.sleep(300)
        await self.game_over(channel, database)

    async def join_game(self, message: object) -> None:
        if message.author.id in self.current_players:
            await message.reply("You're already in the queue to play!")
            return

        self.current_players.append(message.author.id)
        await message.reply("You're in the game! There are currently {} competing players!".format(len(self.current_players)))

    async def notify_next_player_callback(self, message: object, client: object) -> None:
        await asyncio.sleep(configuration.MAX_TURN_LENGTH)
        await self.notify_next_player(message, client)

    async def notify_next_player(self, message: object, client: object) -> None:
        skipped_players = ""
        channel_message = ""
        if self.player_timeout != None:
            self.player_timeout.cancel()
            self.player_timeout = None

        keep_looping = True
        while keep_looping:
            self.current_player_index = (
                self.current_player_index + 1) % len(self.current_players)
            if self.current_player_index in self.skipped_indices:
                skipped_players += "<@{}> ".format(
                    self.current_players[self.current_player_index])
                self.skipped_indices.pop(0)
                self.current_player_index = (
                    self.current_player_index + 1) % len(self.current_players)
            keep_looping = self.current_player_index in self.skipped_indices

        if len(skipped_players) > 0:
            channel_message = "Skipped Players {};".format(skipped_players)

        channel_message += "It's your turn <@{}>! You've got {} seconds. Total game time is: {} minutes.\n{}".format(
            self.current_players[self.current_player_index], configuration.MAX_TURN_LENGTH, configuration.GLOBAL_NO_RESPONSE_TIMEOUT // 60, self.show_hangman())
        self.player_timeout = bot.BotLoop.create_task(
            self.notify_next_player_callback(message, client))

        bot_channel = await client.fetch_channel(configuration.BOT_PLAYGROUND_CHANNEL_ID)
        await bot_channel.send(channel_message)

        return

    async def play(self, message: object, database: object, client: object) -> None:
        if message.author.id != self.current_players[self.current_player_index]:
            return

        game_message = message.content[len(
            string_helpers.prepend_message_char('game')):].strip()

        if game_message == None:
            await message.reply("You need something to guess!")
            return

        game_message = game_message.lower()

        if game_message in self.bad_guesses or game_message in self.good_guesses:
            await message.reply("That was already guessed! Try again")
            return

        if len(game_message) > 1:
            if game_message == self.current_answer.lower():
                self.current_word = self.current_answer
            else:
                await message.reply("WRONG!! You lose 1 turn!")
                self.skipped_indices.append(self.current_player_index)
                self.incorrect_attempts += 1
                self.bad_guesses.append(game_message)
        else:
            indices_with_letters = []
            for i in range(0, len(self.current_answer)):
                if self.current_answer[i].lower() == game_message:
                    indices_with_letters.append(i)
            if len(indices_with_letters) > 0:
                for index in indices_with_letters:
                    self.current_word = self.current_word[:index] + \
                        self.current_answer[index] + \
                        self.current_word[index+1:]
                self.good_guesses.append(game_message)
            else:
                await message.reply("WRONG!")
                self.incorrect_attempts += 1
                self.bad_guesses.append(game_message)

        if self.current_word == self.current_answer:
            await self.win_game(message, database)
        elif self.incorrect_attempts == self.max_attempts:
            channel = await client.fetch_channel(configuration.BOT_PLAYGROUND_CHANNEL_ID)
            await self.game_over(channel, database)
        else:
            await self.notify_next_player(message, client)

    async def set_hangman_word(self) -> None:
        random_page_url = "{}pages/random?private_token={}".format(
            configuration.WEBSITE_API_URL, keys.API_PRIVATE_TOKEN)
        page_object = requests.get(random_page_url)
        get_attempt_max = 5
        current_attempt = 1

        while page_object.status_code != 200 and current_attempt < get_attempt_max:
            page_object = requests.get(random_page_url)
            current_attempt += 1

        if page_object.status_code != 200:
            self.current_answer = "ERROR - (code: {}, error: {}".format(
                page_object.status_code, page_object.reason)
            return

        payload = json.loads(page_object.content)
        self.current_answer = payload['pageTitle']
        self.web_url = "{}/games/{}/{}".format(
            configuration.WEBSITE_URL, payload['gameId'], payload['id'])

        for i in range(0, len(self.current_answer)):
            if re.match(r'[a-zA-Z0-9]', self.current_answer[i]) != None:
                self.current_word += '_'
            elif self.current_answer[i] != ' ':
                self.current_word += self.current_answer[i]
            else:
                self.current_word += ' '

        # For debug and verification purposes (in case of bugs and such)
        print(self.current_answer)
        print(self.web_url)

    def show_hangman(self) -> str:
        ascii = '```\n' +\
                '_________\n' +\
                '|       |\n'
        segments = 4  # The number of segments left to the gallows from the ground

        # Draw head if needed
        if self.incorrect_attempts > 0:
            ascii += '|       O\n'
            segments -= 1

        # Draw body and/or arms
        if self.incorrect_attempts == 2:
            ascii += '|       |\n'
            segments -= 1
        elif self.incorrect_attempts == 3:
            ascii += '|      /|\n'
            segments -= 1
        elif self.incorrect_attempts >= 4:
            ascii += '|      /|\\\n'
            segments -= 1

        # Legs
        if self.incorrect_attempts == 5:
            ascii += '|      / \n'
            segments -= 1
        elif self.incorrect_attempts >= 6:
            ascii += '|      / \\\n'
            segments -= 1

        for _ in range(0, segments):
            ascii += '|\n'

        ascii += "\nCurrent word: {}\n".format(' '.join(self.current_word)) +\
                 "\nCurrent bad guesses: {}\n".format(self.bad_guesses) +\
                 '```'
        if self.incorrect_attempts == self.max_attempts or self.current_word == self.current_answer:
            ascii += "The correct answer is {}!\n".format(self.current_answer) +\
                     "To learn more about this, visit {}".format(self.web_url)

        return ascii

    async def start_game_callback(self, message: object, channel: object, database: object, client: object) -> None:
        await self.set_hangman_word()
        await asyncio.sleep(configuration.GAME_START_TIME)

        if self.current_answer.startswith("ERROR"):
            await channel.send("THERE WAS AN ERROR RETRIEVING A WORD: {}".format(self.current_answer))
            database.query(game.Game).filter_by(game_name="Hangman").update(
                values={game.Game.waiting_for_players: False, game.Game.in_progress: False})
        else:
            database.query(game.Game).filter_by(game_name="Hangman").update(
                values={game.Game.waiting_for_players: False, game.Game.in_progress: True})
        database.commit()

        n = len(self.current_players)
        while n > 1:
            n -= 1
            k = floor(random.random() * n)
            self.current_players[k], self.current_players[n] = self.current_players[n], self.current_players[k]

        self.current_player_index = len(self.current_players) - 1
        await self.notify_next_player(message, client)

        self.global_timeout = bot.BotLoop.create_task(
            self.game_over_callback(channel, database))

    async def start_game(self, message: object, database: object, client: object) -> None:
        self.client = client  # Discord Client
        self.current_players = []  # Who's playing
        self.current_word = ""  # What to show players
        self.current_answer = ""  # The correct answer for players to find
        self.incorrect_attempts = 0  # Number of attempts that have been incorrect
        self.max_attempts = 6  # Maximum attempts before the game ends
        self.bad_guesses = []  # Which letters/words players guessed wrong
        self.good_guesses = []  # Which lettesr/words players guessed correctly
        self.current_player_index = 0  # Which player is currently guessing
        self.skipped_indices = []  # Indices for players that skip due to wrong words guessed
        self.web_url = ""  # URL from the API
        self.global_timeout = None  # When the game ends because no one's responding
        # when it's the next player's turn because someone took too long to reply
        self.player_timeout = None

        self.current_players.append(message.author.id)

        bot_channel = await client.fetch_channel(configuration.BOT_PLAYGROUND_CHANNEL_ID)
        if bot_channel != None:
            bot.BotLoop.create_task(self.start_game_callback(
                message, bot_channel, database, client))

            embed = discord.Embed(
                color=3329330,
                title='The game will begin soon! Here are the instructions'
            )
            embed.set_author(name=client.user.name,
                             icon_url=client.user.avatar_url)
            embed.add_field(name="Guess a letter:", value="`{} [letter]` => Guess a game letter".format(
                string_helpers.prepend_message_char('game')), inline=False)
            embed.add_field(name="Guess the word(s):", value="`{} [word(s)]` => Think you know the word(s)? Type the answer! If you get it wrong, you lose a turn.".format(
                string_helpers.prepend_message_char('game')), inline=False)
            await bot_channel.send(embed=embed)

    async def win_game(self, message: object, database: object) -> None:
        self.clear_timeouts()

        game_obj = database.query(game.Game).filter_by(
            game_name="Hangman").first()
        database.query(user_points.UserPoint).filter((user_points.UserPoint.player_id == message.author.id) & (user_points.UserPoint.game_id == game_obj.id))\
                .update(values={user_points.UserPoint.points: user_points.UserPoint.points + len(self.current_answer)})
        database.query(game.Game).filter_by(game_name="Hangman").update(
            values={game.Game.waiting_for_players: False, game.Game.in_progress: False})
        database.commit()

        await message.reply("Holy moly, you got it!! You just got {} points!\n{}".format(len(self.current_answer), self.show_hangman()))
        return
