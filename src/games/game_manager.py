from sqlalchemy.sql.functions import user
from src.config import configuration
from src.games import hangman
from src.helpers import string_helpers
from src.models import game, user_points

games_available = {
    'Hangman': {
        'game': hangman.Hangman(),
        'name': 'Hangman',
        "description": 'Guess the word(s) and get points!',
        "emoji_id": configuration.EMOJIS['Spoon']
    }
}


def register_player(database: object, message: str, game_name: str) -> None:
    """

    Registers a new player "account" for the respective game. If they already have one, it doesn't.

    """

    game_info = database.query(game.Game).filter_by(
        game_name=game_name).first()

    respective_user = database.query(user_points.UserPoint).filter((user_points.UserPoint.player_id == str(
        message.author.id)) & (user_points.UserPoint.game_id == game_info.id)).first()

    if respective_user == None:
        database.add(user_points.UserPoint(
            game_info.id, str(message.author.id), 0))
        database.commit()


async def restart_games(database: object) -> None:
    """

    Restarts the game status in the database

    """
    database.query(game.Game).update(
        values={game.Game.waiting_for_players: False, game.Game.in_progress: False})
    database.commit()
    print('Games all restarted!')


async def handle_game_command(message: object, database: object, client: object) -> None:
    """

    Parses the game command given for the respective game

    """
    game_playing = database.query(game.Game).filter(
        (game.Game.in_progress == True) | (game.Game.waiting_for_players == True)).first()

    if game_playing != None and game_playing.in_progress:
        await games_available[game_playing.game_name]['game'].play(message, database, client)
    else:
        await message.reply("There aren't any games being played right now!")


async def join_game(message: object, database: object, game_name: str) -> None:
    """

    Joins the user in the game session if it hasn't started yet

    """
    requested_game_name = game_name.lower().capitalize()

    if requested_game_name not in games_available:
        await message.reply("I don't recognize that game, sorry :(")
        return

    existing_running_game = database.query(game.Game).filter(
        (game.Game.in_progress == True) | (game.Game.waiting_for_players == True)).first()

    if existing_running_game != None:
        if existing_running_game.game_name == requested_game_name and existing_running_game.in_progress:
            await message.reply("There's already a game of {} being played! Please wait until it's complete.".format(requested_game_name))
        elif existing_running_game.game_name != requested_game_name and existing_running_game.in_progress:
            await message.reply("There's currently a game of {} being played! Please wait until it's complete.".format(existing_running_game.game_name))
        elif existing_running_game.game_name == requested_game_name and existing_running_game.waiting_for_players:
            register_player(database, message, requested_game_name)
            await games_available[requested_game_name]['game'].join_game(message)
    else:
        await message.reply("Someone needs to start a game of {} first!".format(requested_game_name))


async def start_game(message: object, database: object, client: object, game_name: str) -> None:
    """

    Lets the user start a new game if one hasn't already started

    """
    requested_game_name = game_name.lower().capitalize()

    if requested_game_name not in games_available:
        await message.reply("I don't recognize that game, sorry :(")
        return

    existing_running_game = database.query(game.Game).filter(
        (game.Game.in_progress == True) | (game.Game.waiting_for_players == True)).first()

    if existing_running_game != None:
        if existing_running_game.in_progress:
            await message.reply("There's already a game of {} being played! Please wait until it's complete.".format(existing_running_game.game_name))
        elif existing_running_game.waiting_for_players:
            await message.reply("There's already a game of {0} waiting for players! Type `{1}-{0}` to join!".format(existing_running_game.game_name.lower(), string_helpers.prepend_message_char('join')))
        return

    await message.reply(
        "The game for {0} will start soon! Players who want to join, type `{1}-{0}` within the next {2} seconds!"
        .format(requested_game_name.lower(), string_helpers.prepend_message_char('join'), configuration.GAME_START_TIME)
    )

    await games_available[requested_game_name]['game'].start_game(message, database, client)

    database.query(game.Game).filter_by(game_name=requested_game_name).update(
        values={game.Game.waiting_for_players: True})
    database.commit()

    register_player(database, message, requested_game_name)
