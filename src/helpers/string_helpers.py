from src.config import configuration


def prepend_message_char(string: str) -> str:
    """

    Returns a combination of the message character defined in configuration + string

    """
    return '{}{}'.format(configuration.MESSAGE_CHARACTER, string)
